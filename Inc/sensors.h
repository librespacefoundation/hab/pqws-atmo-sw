/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SENSORS_H_
#define SENSORS_H_
#include "stm32l4xx_hal.h"
#include "atmo.h"

#define ADC_CHANNEL_MQ_7 			ADC_CHANNEL_15
#define ADC_CHANNEL_MUON 			ADC_CHANNEL_10
#define ADC_CHANNEL_MICS 			ADC_CHANNEL_8
#define ADC_CHANNEL_TEMP 			ADC_CHANNEL_17

#define MUON_DETECTION_THRESHOLD	120
#define MUON_RESET_THRESHOLD		73

#define SENSOR_ADC_MULTIPLIER		0.805664063

//#define SEA_LEVEL_PRESSURE			10132.5f
#define SEA_LEVEL_PRESSURE			10250.0f

#define BIT(x)   (1 << (x))

typedef struct {
	uint16_t val1;
	uint16_t val2;
	uint16_t val3;
	uint16_t val4;
	CAN_message_id sensor_type;
} pq_atmo_measurement_t;

typedef struct {
	uint16_t pressure;
	int16_t temperature;
	uint16_t humidity;
	uint16_t pressure_altitude;
	CAN_message_id sensor_type;
} pq_atmo_atmospheric_data_t;

typedef struct {
	uint16_t muon_level;
	uint16_t muon_count;
	uint16_t mcu_temp;
	uint16_t gas_CO;
	CAN_message_id sensor_type;
} pq_atmo_sensor_t;

typedef struct {
	uint16_t pressure_hi_alt;
	uint16_t ext_temp1;
	uint16_t ext_temp2;
	uint16_t batt;
	CAN_message_id sensor_type;
} pq_atmo_sensor2_t;

typedef enum {
	POWER_OFF=0,
	POWER_LOW,
	POWER_HIGH,
	POWER_ON
} sensor_power_t;

typedef enum {
	EQUAL = 0,
	GREATER,
	LESS,
	GREATER_EQUAL,
	LESS_EQUAL
} comparison_operator_t;

uint16_t sensor_read_muons();
uint16_t sensor_read_MICS(ADC_HandleTypeDef *adc);
void sensor_power_off();
void sensor_pyro_power(sensor_power_t state);
void sensor_MICS_power(sensor_power_t state);
void sensor_camera_power(sensor_power_t state);
void sensor_buzzer_power(sensor_power_t state);
void sensor_power(CAN_message_id sensor, sensor_power_t power);
uint16_t get_pressure_altitude(uint16_t pressure, int16_t temperature);
#endif /* SENSORS_H_ */
