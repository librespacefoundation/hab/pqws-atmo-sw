/*
 * state_control.h
 *
 *  Created on: Apr 22, 2019
 *      Author: george
 */

#ifndef STATE_CONTROL_H_
#define STATE_CONTROL_H_
#endif /* STATE_CONTROL_H_ */

#include "main.h"
#include "sensors.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "cmsis_os.h"

typedef enum {
	GROUND = 0,
	FLYING,
	LANDING,
	LANDED
} mission_phase_t;

typedef struct {
	uint16_t pyro_alt;
	uint16_t highestLandPoint;
	mission_phase_t mission_phase;
} initialize_data_t;

uint16_t getAltitude();
void updateStateData();
void updateVerticalVelocityData();
void controlCamera(mission_phase_t mission_phase, sensor_power_t action);
void controlBuzzer(mission_phase_t mission_phase, sensor_power_t action);
void updateMissionPhase();
void initStateControl();
void activatePyro();
void controlPyroByAltitude();
void emergencyReleaseUpdate();
void emergencyReleaseCheck();
void requestCommsParameters();
void setInitData(initialize_data_t *data);
void pyroMainTasks();
void updateSystemTime();
