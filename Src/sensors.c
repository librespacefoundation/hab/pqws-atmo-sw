/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sensors.h"
#include <cmsis_os.h>
#include <main.h>
#include <math.h>

extern ADC_HandleTypeDef hadc1;
extern state_data_t stateData;
extern TIM_HandleTypeDef htim2;
const long double cal[] = { -9.085681659276021e-27, 4.6790804314609205e-23,
		-1.0317125207013292e-19, 1.2741066484319192e-16, -9.684460759517656e-14,
		4.6937937442284284e-11, -1.4553498837275352e-08, 2.8216624998078298e-06,
		-0.000323032620672037, 0.019538631135788468, -0.3774384056850066,
		12.324891083404246 };

void sensor_power_off() {
	sensor_pyro_power(POWER_OFF);
	sensor_MICS_power(POWER_OFF);
}

/**
 * Read raw adc channel value
 * @param adc
 * @param channel
 * @return
 */
uint16_t sensor_get_raw(ADC_HandleTypeDef *adc, uint32_t channel) {
	uint16_t value=0;
	ADC_ChannelConfTypeDef sConfig;
	sConfig.Channel = channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(adc, &sConfig) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_ADCEx_Calibration_Start(adc, ADC_SINGLE_ENDED) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_ADC_Start(adc) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_ADC_PollForConversion(adc, 10) != HAL_OK) {
		Error_Handler();
	} else {
		value = HAL_ADC_GetValue(adc);
	}
	return value;
}

void sensor_power(CAN_message_id sensor, sensor_power_t power){
	switch (sensor) {
	case PQ_ATMO_MUON:
		sensor_PYRO_power(power);
		break;
	case PQ_ATMO_MICS:
		sensor_MICS_power(power);
		break;
	default:
		break;
	}
}


/**
 * Set PYRO sensor power state
 * @param state
 */
void sensor_pyro_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
		HAL_GPIO_WritePin(PYRO_GPIO_Port, PYRO_Pin, GPIO_PIN_RESET);
		stateData.system_state |= BIT(PYRO_POWER);
		break;
	case POWER_OFF:
		HAL_GPIO_WritePin(PYRO_GPIO_Port, PYRO_Pin, GPIO_PIN_SET);
		stateData.system_state &= ~BIT(PYRO_POWER);
		break;
	default:
		break;
	}
}

/**
 * Set camera sensor power state
 * @param state
 */
void sensor_camera_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
		HAL_GPIO_WritePin(Camera_GPIO_Port, Camera_Pin, GPIO_PIN_RESET);
		stateData.system_state |= BIT(CAMERA_POWER);
		break;
	case POWER_OFF:
		HAL_GPIO_WritePin(Camera_GPIO_Port, Camera_Pin, GPIO_PIN_SET);
		stateData.system_state &= ~BIT(CAMERA_POWER);
		break;
	default:
		break;
	}
}

void sensor_buzzer_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
		HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
		stateData.system_state |= BIT(BUZZER_POWER);
		break;
	case POWER_OFF:
		HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
		stateData.system_state &= ~BIT(BUZZER_POWER);
		break;
	default:
		break;
	}
}

/**
 * Set MICS sensor power state
 * @param state
 */
void sensor_MICS_power(sensor_power_t state) {
	switch (state) {
	case POWER_ON:
	case POWER_LOW:
	case POWER_HIGH:
		HAL_GPIO_WritePin(MICS_V_GPIO_Port, MICS_V_Pin, GPIO_PIN_RESET);
		stateData.system_state |= BIT(MICS_POWER);
		break;
	case POWER_OFF:
		HAL_GPIO_WritePin(MICS_V_GPIO_Port, MICS_V_Pin, GPIO_PIN_SET);
		stateData.system_state &= ~BIT(MICS_POWER);
		break;
	default:
		break;
	}
}

/**
 * Get O3 sensor voltage
 * @param adc
 * @return milivolts
 */
uint16_t sensor_read_muons() {
	uint16_t value;

	// Wait for reset
	do {
		value = sensor_get_raw(&hadc1, ADC_CHANNEL_MUON);
		osDelay(50);
	} while (value > MUON_RESET_THRESHOLD);

	// Wait for detection
	do {
		value = sensor_get_raw(&hadc1, ADC_CHANNEL_MUON);
//		osDelay(10);
	} while (value < MUON_DETECTION_THRESHOLD);

	//Get peak voltage
//	do {
//		max = value;
//		value = sensor_get_raw(&hadc1, ADC_CHANNEL_MUON);
//	} while (value > max);

	return value*SENSOR_ADC_MULTIPLIER;
}

/**
 * Get O3 sensor voltage
 * @param adc
 * @return milivolts
 */
uint16_t sensor_read_MICS(ADC_HandleTypeDef *adc) {
	uint16_t value;
	//TODO: Sensor activate sequence
	value = sensor_get_raw(adc, ADC_CHANNEL_MICS);
	//TODO: convert to PPM
	return value * SENSOR_ADC_MULTIPLIER;
}

// This function converts the measured ADC value to a SiPM voltage via the calibration array
float get_sipm_voltage(float adc_value)
{
  float voltage = 0;
  uint8_t i;

  for (i = 0; i < (sizeof(cal)/sizeof(float)); i++) {
    voltage += cal[i] * pow(adc_value,(sizeof(cal)/sizeof(float)-i-1));
    }
    return voltage;
}

uint16_t get_pressure_altitude(uint16_t pressure, int16_t temperature) {
	float h;
	h = ((powf((SEA_LEVEL_PRESSURE / pressure), 0.19022256) - 1) * (temperature + 273.15)) * 153.846153846;
	return rint(h);
}
