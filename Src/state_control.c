/*
 * state_control.c
 *
 *  Created on: Apr 22, 2019
 *      Author: george
 */

#include "state_control.h"
extern pq_atmo_atmospheric_data_t atmosphericData;
extern osSemaphoreId bme_read_controlHandle;
extern osMessageQId CANTxQueueHandle;
state_data_t 		stateData;
mission_phase_t 	missionPhase;
uint16_t 			pyro_altitude = 3000;
uint16_t 			highest_land_point = 1000;
#define 			PYRO_SECONDS 				10
#define 			CHUTE_DEPLOY_DIFFERENCE_MIN 5
uint8_t				pyro_deploy = 0;
uint8_t				pyro_activate = 0;
time_t				pyro_activate_time;
int8_t				pyro_average_velocity = 0;
int16_t				velocityBuffer[4];
uint8_t				velocityBufferCounter = 0;
uint16_t			lastAlt = 0;
uint16_t			currAlt = 0;
uint16_t			launchAlt = 0;
uint32_t			system_time = 0;
flight_phase_t 		emergencyRelease[2];
uint8_t 			emergencyCounter = 0;
int8_t 				average_velocity;

uint16_t getAltitude(){
	osSemaphoreWait(bme_read_controlHandle, osWaitForever);
	uint16_t temp_alt = atmosphericData.pressure_altitude;
	osSemaphoreRelease(bme_read_controlHandle);
	return temp_alt;
}

void updateSystemTime(){
	system_time = HAL_GetTick() / 1000;
}

void initStateControl(){
	requestCommsParameters();
	launchAlt = getAltitude();
	emergencyRelease[0] = FLOATING;
	emergencyRelease[1] = FLOATING;
	for(uint8_t i = 0; i < 4; i++){
		velocityBuffer[i] = 0;
	}
}

void updateStateData(){
	average_velocity = 0;
	static uint8_t i;
	for(i = 0; i < 4; i++)
		average_velocity += velocityBuffer[i];
	average_velocity /= 4;

	int8_t temp = 0;
	int8_t minvel = velocityBuffer[0];
	int8_t maxvel = velocityBuffer[0];

	for(i = 0; i < 4; i++)
	{
		if(velocityBuffer[i] > maxvel) maxvel = velocityBuffer[i];
		if(velocityBuffer[i] < minvel) minvel = velocityBuffer[i];
		if(velocityBuffer[i] < 0) temp--;
		else if(velocityBuffer[i] > 0) temp++;
	}

	int8_t vel_variance = maxvel - minvel;

	/* State decision */
	if((abs(average_velocity) < 2)&&(vel_variance < 2))
		stateData.flightPhase = FLOATING;
	else if((temp == 4)&&(abs(vel_variance) < 500))//VARIANCE FOR VACUUM (50 for launch)
		stateData.flightPhase = ASCENDING;
	else if((temp == -4)&&(abs(vel_variance) < 500))//VARIANCE FOR VACUUM (50 for launch)
		stateData.flightPhase = DESCENDING;
	else
		if(vel_variance > 42)
			stateData.flightPhase = FLIGHT_STATE_INVALID;
		else stateData.flightPhase = FLOATING;

	xQueueSend(CANTxQueueHandle, (void *) &stateData, 1000);
	pq_atmo_measurement_t sys_time_msg;
	memcpy(&sys_time_msg, &system_time, 4);
	sys_time_msg.sensor_type = PQ_ATMO_SYSTEM_TIME;
	xQueueSend(CANTxQueueHandle, (void *) &sys_time_msg, 1000);

}

void updateVerticalVelocityData(){
	currAlt = getAltitude();
	stateData.verticalVelocity = currAlt - lastAlt;
	lastAlt = currAlt;
	velocityBuffer[velocityBufferCounter%4] = stateData.verticalVelocity;
	velocityBufferCounter++;
}

void controlCamera(mission_phase_t mission_phase, sensor_power_t action){
	if(missionPhase == mission_phase)
		sensor_camera_power(action);
}

void controlBuzzer(mission_phase_t mission_phase, sensor_power_t action){
	if(missionPhase == mission_phase)
			sensor_buzzer_power(action);
}

void activatePyro(){
	if(pyro_activate==0){
		sensor_pyro_power(POWER_ON);
		pyro_activate = 1;
		pyro_activate_time = system_time;
		pyro_average_velocity = average_velocity;
	}
}

void disablePyro(){
	sensor_pyro_power(POWER_OFF);
}

void updateMissionPhase(){
	if((missionPhase == GROUND)&&(stateData.flightPhase == ASCENDING)&&(currAlt > launchAlt + 100))
		missionPhase = FLYING;
	if((missionPhase == FLYING)&&(stateData.flightPhase == DESCENDING)&&(currAlt > pyro_altitude - 1500))
		missionPhase = LANDING;
	if((missionPhase == LANDING)&&(stateData.flightPhase == FLOATING)&&(currAlt <= highest_land_point))
		missionPhase = LANDED;
}

void pyroMainTasks(){
	controlPyroByAltitude();
	emergencyReleaseUpdate();
	emergencyReleaseCheck();
	if((system_time - pyro_activate_time > 30)&&(pyro_activate == 1)){
		disablePyro();
	}
	if(pyro_activate == 1){
		if((stateData.flightPhase == DESCENDING)&&(abs(average_velocity) < (abs(pyro_average_velocity) - CHUTE_DEPLOY_DIFFERENCE_MIN))){
			pyro_deploy = 1;
			disablePyro();
		}
	}
}

void controlPyroByAltitude(){
	if(currAlt > pyro_altitude)
			activatePyro();
}

void emergencyReleaseUpdate(){
	emergencyRelease[emergencyCounter%2] = stateData.flightPhase;
	emergencyCounter++;
}

void emergencyReleaseCheck(){
	if((emergencyRelease[0] == DESCENDING)&&(emergencyRelease[1] == DESCENDING)){
		activatePyro();
	}
}

void requestCommsParameters(){
	pq_atmo_measurement_t initRequest;
	initRequest.sensor_type = PQ_ATMO_INIT_REQUEST;
	xQueueSend(CANTxQueueHandle, (void *) &initRequest, 1000);
	osDelay(2000);
}

void setInitData(initialize_data_t *data){
	pyro_altitude = data->pyro_alt;
	highest_land_point = data->highestLandPoint;
	missionPhase = data->mission_phase;
}
